from app import db,lm
# import datetime
from sqlalchemy.dialects.mysql import TIMESTAMP
from datetime import datetime
# from flask_login import LoginManager, UserMixin, login_user, logout_user,current_user
from flask_login import UserMixin
class Details(UserMixin,db.Model):
  id = db.Column(db.Integer, primary_key=True)
  username = db.Column(db.String(500))
  email = db.Column(db.String(500))
  password=db.Column(db.String(500))
  token=db.Column(db.String(500))
  social_id = db.Column(db.String(500),nullable=True,unique=True)
  nickname = db.Column(db.String(500),nullable=True)
  login_at = db.Column(db.DateTime, default=datetime.now())
  child = db.relationship('Child', backref='Details',
                                lazy='dynamic')

  # def __init__(self, username, email, password, token, social_id, nickname,latitude,longitude,timestamp,timeout_flag,speed,created_at):
  def __init__(self, username, email, password, token, social_id, nickname,login_at):
   self.username = username
   self.email = email
   self.password = password
   self.token = token
   self.social_id = social_id
   self.nickname = nickname
   self.login_at=login_at
   # self.child=child


class Child(UserMixin,db.Model):
  id = db.Column(db.Integer, primary_key=True)
  latitude = db.Column(db.String(500))
  longitude = db.Column(db.String(500))
  timestamp = db.Column(db.String(500))
  timeout_flag = db.Column(db.Integer)
  speed = db.Column(db.String(500))
  trackdate = db.Column(db.String(500))
  details_id= db.Column(db.Integer, db.ForeignKey('details.id'))
  # details = db.relationship('Details', backref='Child',lazy='dynamic')
  def __init__(self,latitude,longitude,timestamp,timeout_flag,speed,trackdate,details_id):
   self.latitude = latitude
   self.longitude = longitude
   self.timestamp = timestamp
   self.timeout_flag = timeout_flag
   self.speed = speed
   self.trackdate = trackdate
   self.details_id = details_id
   