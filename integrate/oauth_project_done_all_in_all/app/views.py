from flask import render_template, request, session, abort,flash, redirect, url_for
from app import app, db, lm, mail,s
# from app import *
from flask_mail import Message
from app.models import Details,Child
from datetime import datetime
# import datetime
import jwt
import bcrypt
import re
import os
import json
from bcrypt import hashpw, gensalt
from flask_login import current_user,login_user,logout_user
from oauth import OAuthSignIn
from itsdangerous import SignatureExpired
# from itsdangerous import URLSafeTimedSerializer, SignatureExpired
from nocache import nocache
@lm.user_loader
def load_user(id):
    return Details.query.get(int(id))

@app.route('/main_index_page')
@nocache
def mainpage():
	if not current_user.is_anonymous:
		flash('Welcome')
		return render_template('mainpage.html')
	# flash('please login')
	return redirect(url_for('login_page_render'))
@app.route('/login_render')
def login_page_render():
	if not current_user.is_anonymous:
		flash('You are already logged in')
		return redirect(url_for('mainpage'))
	return render_template('login.html')


@app.route('/reset_link',methods=['POST','GET'])
def reset_link():
	if request.method == 'GET':
		sending_path=request.url
		return render_template('email.html', path=sending_path)
	if request.method == 'POST':
		email=request.form['email']
		match=re.match('[\w]+@[\w.]+[\w]',email)
		if not email:
			flash('Please provide your email_id')
			return render_template('email.html')
		elif match==None:
			flash('Please provide a valid email')
			return render_template('email.html') 
		user=Details.query.filter_by(email=email).first()
		if not user:
			flash('You didn\'t have an account')
			return redirect(url_for('signup'))
		token =s.dumps(email,salt='reset_password')
		msg = Message('Reset_password', sender = 'cjjayam95@gmail.com', recipients = [user.email])
		link =url_for('pwdreset',token=token,_external=True)
		msg.body = "Hello,{}! this is your password resetlink{}".format(user.username,link)
		mail.send(msg)
		return "verify your email to reset your password"
@app.route('/new_password/<token>')
def pwdreset(token):
	try:
		email=s.loads(token,salt='reset_password',max_age=60)
		session['email'] = email
	except SignatureExpired:
		return "The token is expired"
	return render_template('pwdreset.html')
@app.route('/storing_new_pwd',methods=['POST','GET'])
def rm_old_add_new():
	if request.method == 'POST':
		email=session['email']
		# print email
		PASSWORD = request.form['password']
		if not PASSWORD:
			flash('Please provide your new password')
			return render_template('pwdreset.html')
		PASSWORD = PASSWORD.encode('utf-8')
		pwd_hashed= hashpw(PASSWORD, gensalt())
		user=Details.query.filter_by(email=email).first()
		user.password=pwd_hashed
		db.session.commit()
		session.pop('email',None)
		# print email
		flash('Login with your new password')
		return redirect(url_for('login_page_render'))
	
@app.route('/login_validate', methods=['POST'])
def do_admin_login():
	EMAIL = request.form['email']
	PASSWORD = request.form['password']
	PASSWORD = PASSWORD.encode('utf-8')
	retrive_user_details= Details.query.filter_by(email=EMAIL).first()
	if not EMAIL or not PASSWORD:
			flash('Please enter all the fields', 'error')
	elif not retrive_user_details:
		flash('User does not exit')
	elif retrive_user_details.password is None:
		flash('you have signed up with one of your social account! , please continue with your social account')
	else:
		pw=str(retrive_user_details.password)
		if hashpw(PASSWORD, pw) == pw:
			# flash('Welcome')
			login_user(retrive_user_details, True)
			return redirect(url_for('mainpage'))
		else:
			flash('wrong password!')
	return redirect(url_for('login_page_render'))

@app.route("/logout1")
def logout():
	# if  current_user.is_authenticated:
	logout_user()
	return redirect(url_for('login_page_render'))

@app.route('/')
def signup():
	if not current_user.is_anonymous:
		flash('You are already logged in')
		return redirect(url_for('mainpage'))
	return render_template('submit.html')

@app.route('/signup_validate' , methods=['POST', 'GET'])
def submit():
	if request.method == 'POST':
		# post = Details.query.get(1)
		# print post
		a=request.form['username']
		b=request.form['password']
		c=request.form['email']
		retrive_from_details= Details.query.filter_by(email=c).first()
		match=re.match('[\w]+@[\w.]+[\w]',c)
		if not a or not b or not c:
			flash('Please enter all the fields', 'error')		
		elif match == None:
			flash('Bad email id')
		elif retrive_from_details is not None and retrive_from_details.email == c:
			flash('user already exit,please login to your account')
			return redirect(url_for('login_page_render')) 
		else:
			b = b.encode('utf-8')
			hashed = hashpw(b, gensalt())
			dic={
			'username':a,
			'email':c
			}
			token=jwt.encode(dic,'secret',algorithm='HS256')
			new_user=Details( a, c, hashed,token,None,None,login_at=datetime.now())
			db.session.add(new_user)
			db.session.commit()
			# flash('Welcome')
			login_user(new_user, True)
			return redirect(url_for('mainpage'))
	return redirect(url_for('signup'))

@app.route('/authorize/<provider>')
def oauth_authorize(provider):
    if not current_user.is_anonymous:
        return redirect(url_for('mainpage'))
    oauth = OAuthSignIn.get_provider(provider)
    return oauth.authorize()

@app.route('/callback/<provider>',methods=['POST', 'GET'])
def oauth_callback(provider):
	if request.method == 'GET':
		if not current_user.is_anonymous:
			return redirect(url_for('mainpage'))
		oauth = OAuthSignIn.get_provider(provider)
		provider_name=str(oauth.provider_name)
		username, email, social_id = oauth.callback()
		user=Details.query.filter_by(social_id=social_id).first()
		if not user:
			if email is None:
				required_path=request.url
				session['USERNAME'] = username
				session['SOCIAL_ID'] = social_id
				flash('Your social account does not provide email_id')
				return render_template('email.html', path=required_path)
			if username is None or username == "":
				nickname = email.split('@')[0]
				json1={
				'username':nickname,
				'email':email
				}
				token1=jwt.encode(json1,'secret',algorithm='HS256')
				user=Details(username=None,email=email,password=None,token=token1,social_id=social_id, nickname=nickname, login_at=datetime.now())
				db.session.add(user)
				db.session.commit()
				login_user(user, remember=True)
				return redirect(url_for('mainpage'))
			json2={
			'username':username,
			'email':email
			}
			token2=jwt.encode(json2,'secret',algorithm='HS256')
			user=Details(username=username,email=email,password=None,token=token2,social_id=social_id, nickname=None,login_at=datetime.now())
			db.session.add(user)
			db.session.commit()	
		login_user(user, remember=True)
		return redirect(url_for('mainpage'))

	if request.method == 'POST':
		email=request.form['email']
		username=session['USERNAME']
		social_id=session['SOCIAL_ID']
		# print ('')
		# print username
		# print social_id
		# print ('')
		match=re.match('[\w]+@[\w.]+[\w]',email)
		if not email:
			flash('Please provide your email_id')
			return render_template('email.html')
		elif match==None:
			flash('Please provide a valid email')
			return render_template('email.html')
		if username is None or username == "":
			nickname = email.split('@')[0]
			json3={
			'username':nickname,
			'email':email
			}
			token3=jwt.encode(json3,'secret',algorithm='HS256')
			user=Details(username=None,email=email,password=None,token=token3,social_id=social_id, nickname=nickname,login_at=datetime.now())
			db.session.add(user)
			db.session.commit()
			login_user(user, remember=True)
			return redirect(url_for('mainpage'))
		json4={
			'username':username,
			'email':email
			}
		token4=jwt.encode(json4,'secret',algorithm='HS256')
		user=Details(username=username,email=email,password=None,token=token4,social_id=social_id, nickname=None,login_at=datetime.now())
		db.session.add(user)
		db.session.commit()
		# print username
		session.pop('USERNAME',None)
		session.pop('SOCIAL_ID',None)
		
		# print social_id
		login_user(user, remember=True)
		return redirect(url_for('mainpage'))


	# from flask import Flask,render_template,request
# from flask_googlemaps import GoogleMaps
# import webbrowser
# app =Flask(__name__,template_folder="templates")
# app = Flask(__name__)
# app.config['GOOGLEMAPS_KEY']='AIzaSyCTvweI36MruTx_0QMFzaPMvLOwiLhOvPs'
# GoogleMaps(app)
@app.route('/integration')
def geomap1():
	return render_template('integrate(confused).html')
@app.route('/only/direction')
def geomap2():
	return render_template('onlydic.html')
@app.route('/visited/places')
def geomap3():
	return render_template('visited_place.html')
@app.route('/watchlocations')
def geomap4():
	return render_template('watch_cleare.html')
@app.route('/watch/locations/Simple')
def geomap5():
	return render_template('watch_simple_clear.html')
@app.route('/this_is_main')
def geomap6():
	return render_template('mainpage_1.html')
@app.route('/simplemap')
def geomap7():
	lat=13.0827
	lon=79.2707
	# return render_template('location.html')
	return render_template('simplemap.html',lat=lat,lon=lon)
@app.route('/current/location')
def geomap8():
	return render_template('currentlocation.html')
@app.route('/map/marker')
def geomap9():
	lat=13.0827
	lon=80.2707
	return render_template('map_with_marker.html',lat=lat,lon=lon)

@app.route('/fixed/current/location')
def geomap10():
	return render_template('fixed_to_current.html')

@app.route('/only/marker')
def geomap11():
	return render_template('onclick_marker.html')	

@app.route('/js/array/push')
def geomap12():
	return render_template('location_store_in_an _array.html')


@app.route('/both_end/direction/drawing/')
def geomap13():
	return render_template('end_to_end_direction_and_drawling.html')

@app.route('/value/changes')
def geomap14():
	return render_template('value_changes.html')
@app.route('/uber')
def geomap15():
	return render_template('uber_oneClick_twoFunc_call.html')	
# @app.route('/jesus')
# def geomap16():
# 	return render_template('jesus.html')	
@app.route('/drawing_tool')
def geomap18():
	return render_template('drawing_tool_in_map.html')
@app.route('/polyline')
def geomap19():
	return render_template('polyline_add_and_remove.html')

@app.route('/postmethod', methods = ['POST'])
def get_post_javascript_data():
	jsdata = request.form['data']
	geodict =json.loads(jsdata)
	# print geodict['lat']
	d=datetime.now()
	# print d.date()
	# print d
	user=Child(latitude=geodict['lat'],longitude=geodict['lng'],timestamp=geodict['timestamp'],timeout_flag=geodict['timeout_flag'],speed=geodict['speed'], trackdate=d.date() ,details_id=current_user.id)
	db.session.add(user)
	db.session.commit()
	print "succes.............."
	# user=Details.query.filter_by(username='Saravana Prakash').first()
	# print user.email
	# for i in user.child.all():
	# 	print i.latitude


    # if geodict['lat'] and geodict['lng'] and geodict['timestamp'] == "":
    # 	print ("starting time itself timeout occurs so that corresponding values are taken from empty textfield")

    # print geodict['lat']
    # print geodict['timeout_flag']
    # print geodict['speed'] 
    # if  geodict['lat'] and geodict['lng'] and geodict['timestamp'] and geodict['timeout_flag']:
    # if geodict['timeout_flag']==0:
    # if geodict['timeout_flag'] is None or geodict['timeout_flag'] == "":
    # if geodict['speed'] == "":
    # 	print("hello")
    # 	print geodict['timeout_flag']
    # 	print geodict['lat']
    # else:
    # 	print("no..")
    # 	print geodict['lat']
    # 	print geodict['timeout_flag']
    # child.Details
	return 'returnsomething'


# @app.route('/getpythondata')
# def get_python_data():
# 	pythondata={'a':'hi','b':'hello'}
# 	return json.dumps(pythondata)


@app.route('/dashboard')
def dashboard():
	dropdown_list = []
	for assurance in Details.query.distinct(Details.email):
		dropdown_list.append(assurance.email)
	# print dropdown_list
	# print 'hello'
	return render_template('dashboard.html',dropdown_list=dropdown_list)


@app.route('/sendpythondata',methods=['POST', 'GET'])
def get_python_data():
	
	if request.method == 'POST':
		arr=[]
		date=request.form['date']
		email=request.form['email']
		print date+'done..................'
		print email+"done................."
		if not date:
			flash("Please choose the date")
			return redirect(url_for('dashboard'))
		else:
			user=Details.query.filter_by(email=email).first()
			print user.email

			for i in user.child.all():
				if date == i.trackdate:
					# print i.details_id
					# print i.timeout_flag
					main_dict={}
					main_dict['latitude']=i.latitude
					main_dict['longitude']=i.longitude
					main_dict['timeout_flag']=i.timeout_flag
					arr.append(main_dict)
			# return json.dumps(arr)
			print arr
			print "done"
			return render_template('user_routes.html',coords_list=arr)
			
				# print i.latitude

	return 'process done'


